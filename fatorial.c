#include "fatorial.h"

int fatorial(int x){
	int fat, n;
	n = x;
	if (n >= 1){
     	for(fat = 1; n > 1; n = n - 1)
    	fat = fat * n;
    	return fat;
	}
	return -1;	
}